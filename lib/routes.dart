import 'package:emporio_do_diabetico/app/views/pages/login_register_page/login.dart';
import 'package:flutter/cupertino.dart';
import 'app/views/pages/home_page/home.dart';
import 'app/views/pages/login_register_page/homeLoginRegister.dart';
import 'app/views/pages/login_register_page/register.dart';
import 'app/views/pages/splash_screen/splash_screen.dart';

class Routes {
  static var initialRoute = '/splash';

  static var routes = <String, WidgetBuilder>{
    '/home': (BuildContext context) => Home(
          currentIndex: 0,
        ),
    '/login': (BuildContext context) => HomeLoginRegister(currentIndex: 0),
    '/register': (BuildContext context) => HomeLoginRegister(currentIndex: 1),
    '/splash': (BuildContext context) => SplashScreen()
  };
}
