import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:emporio_do_diabetico/app/views/pages/checkout/DeliveryInfo.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:emporio_do_diabetico/routes.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'app/views/shared/constants/app_colors.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: const [Locale('pt', 'BR')],
      title: 'Empório do Diabético',
      theme: ThemeData(
        primaryColor: AppColors.green,
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: Routes.initialRoute,
      routes: Routes.routes,
    );
  }
}
