import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  int? id;
  String? name;
  String? email;
  String? cpf;
  String? phoneNumber;
  String? password;
  DateTime? registrationDate;
  bool isAdmin = false;

  User(this.id, this.name, this.email, this.cpf, this.phoneNumber, this.password, this.registrationDate, this.isAdmin);

  @override
  String toString() {
    return 'User{id: $id, name: $name, email: $email, cpf: $cpf, phoneNumber: $phoneNumber, registrationDate: $registrationDate, isAdmin: $isAdmin}';
  }

  Map toJson() => {
    'id': id,
    'name': name,
    'email': email,
    'cpf': cpf,
    'phoneNumber': phoneNumber,
    'isAdmin' : isAdmin,
  };
}

User docToUser(var doc) {

  var id = doc['id'];
  var name = doc['name'];
  var email = doc['email'];
  var cpf = doc['cpf'];
  var phoneNumber = doc['phoneNumber'];
  var password = "";
  DateTime? registrationDate = null;
  if(doc['registrationDate'] != null){
    Timestamp timestamp = doc['registrationDate'];
    registrationDate = timestamp.toDate();
  }
  var isAdmin = doc['isAdmin'] ?? false;
  User user = User(id,name,email,cpf,phoneNumber,password,registrationDate, isAdmin);
  return user;
}
