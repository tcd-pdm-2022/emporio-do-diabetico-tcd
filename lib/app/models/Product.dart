class Product {
  int? id;
  String? name;
  double? price;
  String? description;
  String? imageURL;
  String? imagePATH;
  
  Product(this.id, this.name, this.price, this.description, this.imageURL);

  @override
  String toString() {
    return 'Product{id: $id, name: $name, price: $price, description: $description, imageURL: $imageURL}';
  }

  Map toJson() => {
    'id': id,
    'name': name,
    'price': price,
    'description': description,
    'imageURL': imageURL
  };
}

Product docToProduct(var doc) {
  var id = doc['id'];
  var name = doc['name'];
  double price = double.parse(doc['price'].toString());
  var description = doc['description'];
  var imageURL = doc['imageURL'];
  Product product = Product(id,name,price,description,imageURL);
  return product;
}