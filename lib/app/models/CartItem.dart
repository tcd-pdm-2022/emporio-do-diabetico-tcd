import 'Product.dart';

class CartItem {
  int productId;
  int quantity;
  Product? product;

  CartItem(this.productId, this.quantity, [this.product]);

  @override
  String toString() {
    return 'CartItem{productId: $productId, quantity: $quantity, product: $product}';
  }

  Map toJson() => {
    'productId': productId,
    'quantity': quantity,
    'product': product
  };
}

docToCartItem(var doc){
  var productId = doc['productId'];
  var quantity = doc['quantity'];
  var product = docToProduct(doc['product']);
  CartItem cartItem = CartItem(productId, quantity, product);
  return cartItem;
}