class CreditCard{
  String cardNumber;
  String name;
  String cpf;
  String expirationDate;
  String cvv;

  CreditCard(this.cardNumber, this.name, this.cpf, this.expirationDate, this.cvv);

  @override
  String toString() {
    return ("CreditCard{cardNumber: $cardNumber, name: $name, cpf: $cpf, expirationDate: $expirationDate, cvv: $cvv");
  }

  Map toJson() => {
    'cardNumber': cardNumber,
    'name': name,
    'cpf': cpf,
    'expirationDate': expirationDate,
    'cvv': cvv
  };
}

docToCreditCard(var doc){
  var cardNumber = doc['cardNumber'];
  var name = doc['name'];
  var cpf = doc['cpf'];
  var expirationDate = doc['expirationDate'];
  var cvv = doc['cvv'];
  CreditCard creditCard = CreditCard(cardNumber, name, cpf, expirationDate, cvv);
  return creditCard;
}