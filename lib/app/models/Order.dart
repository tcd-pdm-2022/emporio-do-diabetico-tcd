import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:emporio_do_diabetico/app/models/Address.dart';
import 'package:emporio_do_diabetico/app/models/CartItem.dart';
import 'package:emporio_do_diabetico/app/models/CreditCard.dart';

import 'Product.dart';
import 'User.dart';

class Order{
  int? id;
  int? clientId;
  User? cliente;
  double? value;
  Address? address;
  CreditCard? creditCard;
  List<CartItem>? cartItems;
  String? paymentOption;
  DateTime? orderDate;

  Order(this.id, this.clientId, this.cliente, this.value, this.address, this.creditCard, this.cartItems, this.paymentOption, this.orderDate);

  @override
  String toString() {
    return 'Order{id: $id, clientId: $clientId, cliente: $cliente, value: $value, address: $address, creditCard: $creditCard, cartItems: $cartItems, paymentOption: $paymentOption, orderDate: $orderDate}';
  }
}

Order docToOrder(var doc){
  Timestamp timestamp = doc['orderDate'];
  List<CartItem> cartItems = [];
  List<dynamic> cartItemsDoc = jsonDecode(doc['cartItems']);
  for (var element in cartItemsDoc) {
    cartItems.add(docToCartItem(element));
  }

  var id = doc['id'];
  var clientId = doc['clientId'];
  User cliente = docToUser(jsonDecode(doc['cliente']));//User(10,"Joao","joao@gmail.com","111.222.333-44","(34) 98888-8888","",null);
  var value = doc['value'];
  var address = docToAddress(jsonDecode(doc['address']));
  var creditCard = docToCreditCard(jsonDecode(doc['creditCard']));
  var paymentOption = doc['paymentOption'];
  var orderDate = timestamp.toDate();
  Order order = Order(id, clientId, cliente, value, address, creditCard, cartItems, paymentOption, orderDate);
  return order;
}