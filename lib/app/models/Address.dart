class Address{
  String address;
  String number;
  String? complement;
  String city;
  String state;
  String postal_code;

  Address(this.address, this.number, this.complement, this.city, this.state, this.postal_code);

  @override
  String toString() {
    return 'Address{address: $address, number: $number, complement: $complement, city: $city, state: $state, postal_code: $postal_code}';
  }

  Map toJson() => {
    'address': address,
    'number': number,
    'complement': complement,
    'city': city,
    'state': state,
    'postal_code': postal_code
  };
}

docToAddress(var doc){
  var address = doc['address'];
  var number = doc['number'];
  var complement = doc['complement'];
  var city = doc['city'];
  var state = doc['state'];
  var postal_code = doc['postal_code'];
  Address add = Address(address,number,complement,city,state,postal_code);
  return add;
}