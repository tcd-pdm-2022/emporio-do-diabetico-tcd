import 'package:emporio_do_diabetico/app/models/Product.dart';
import 'package:emporio_do_diabetico/app/views/pages/product_add/ProductAdd_Edit.dart';
import 'package:flutter/material.dart';
import '../../../pages/product_detail/ProductDatail.dart';
import '../../constants/app_constants.dart';
import 'ProductImage.dart';

class ProductCardHomeGridView extends StatelessWidget {
  Product product;
  ProductCardHomeGridView({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: InkWell(
        onTap: () async {
          //TODO Implement product detail page access
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ProductDetail(product: product)));
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: (MediaQuery.of(context).size.width / 2 - 10),
              width: double.infinity,
              child: ProductImage(product: product,),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: ListTile(
                title: Text(
                  product.name ?? "",
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 16),
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 2.0, bottom: 1),
                          child: Text(
                              'R\$ ${product.price?.toStringAsFixed(2) ?? ""}',
                              style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontWeight: FontWeight.w700,
                              )),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
