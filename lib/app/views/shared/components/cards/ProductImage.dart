import 'package:flutter/material.dart';
import '../../../../models/Product.dart';
import '../../constants/app_constants.dart';

class ProductImage extends StatelessWidget {
  Product product;
  ProductImage({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.network(
      product.imageURL ?? defaultimage,
      fit: BoxFit.cover,
      errorBuilder: (context, url, error) => Container(
        color: Colors.grey.withOpacity(0.5),
        child: const Icon(
          Icons.error,
          color: Colors.red,
          size: 50.0,
        ),
      ),
      frameBuilder: (context, child, frame, wasSynchronouslyLoaded) {
        return child;
      },
      loadingBuilder: (context, child, loadingProgress) {
        if (loadingProgress == null) return child;
        return Container(
          color: Colors.grey.withOpacity(0.25),
          child: const Center(child: CircularProgressIndicator()),
        );
      },
    );
  }
}
