import 'package:emporio_do_diabetico/app/models/CartItem.dart';
import 'package:flutter/material.dart';

import '../../../../models/Product.dart';
import 'ProductImage.dart';

Widget ProductCard(context, CartItem item) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Divider(
        height: 2,
      ),
      Padding(
        padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
        child: ListTile(
          trailing: Text('R\$ ${(item.product!.price! * item.quantity).toStringAsFixed(2)}'),
          leading: ClipRRect(
            borderRadius: BorderRadius.circular(5.0),
            child: Container(
              decoration: BoxDecoration(color: Colors.grey.withOpacity(0.5)),
              child: ProductImage(
                  product: item.product!),
            ),
          ),
          title: Text(
            item.product?.name ?? "",
            style: const TextStyle(fontSize: 14),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 2.0, bottom: 1),
                    child: Text("${item.quantity}" + (item.quantity > 1 ? " unidades" : " unidade"),
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontWeight: FontWeight.w700,
                        )),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    ],
  );
}