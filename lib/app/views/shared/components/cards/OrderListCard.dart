import 'package:emporio_do_diabetico/app/models/CartItem.dart';
import 'package:flutter/material.dart';

import '../../../../models/Order.dart';
import '../../../../models/Product.dart';
import '../../../pages/OrderDetails/OrderDetails.dart';
import '../../constants/app_colors.dart';
import 'ProductImage.dart';
import 'package:intl/intl.dart';

Widget OrderListCard(context, Order order) {
  Size screen = MediaQuery.of(context).size;
  double widthCard = screen.width * 1;
  String productResume = _cartItemListToProductResume(order.cartItems);
  String formattedDate = "";

  if(order.orderDate != null)
    formattedDate = DateFormat('kk:mm - dd/MM/yy').format(order.orderDate!);

  return Padding(
      padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => OrderDetails(order: order)));
        },
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              width: 2,
              color: Colors.black,
            ),
            borderRadius: BorderRadius.circular(0),
          ),
          width: widthCard,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(left: 10),
                width: widthCard,
                alignment: Alignment.centerLeft,
                color: AppColors.green,
                child: Text(
                  "Pedido #${order.id}",
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                width: widthCard,
                color: AppColors.blue.withOpacity(0.1),
                padding: EdgeInsets.only(left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        const Text(
                          "Itens: ",
                          maxLines: 1,
                          style: TextStyle(color: Colors.black, fontSize: 19, fontWeight: FontWeight.bold),
                          overflow: TextOverflow.ellipsis,
                        ),
                        Expanded(child:
                        Text(
                          productResume,
                          maxLines: 3,
                          style: TextStyle(color: Colors.black, fontSize: 19),
                          overflow: TextOverflow.ellipsis,
                        ))
                      ],
                    ),
                    Row(
                      children: [
                        const Text(
                          "Pagamento: ",
                          maxLines: 1,
                          style: TextStyle(color: Colors.black, fontSize: 19, fontWeight: FontWeight.bold),
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text(
                          order.paymentOption ?? "",
                          maxLines: 1,
                          style: TextStyle(color: Colors.black, fontSize: 19),
                          overflow: TextOverflow.ellipsis,
                        )
                      ],
                    ),
                    Row(
                      children: [
                        const Text(
                          "Data: ",
                          maxLines: 1,
                          style: TextStyle(color: Colors.black, fontSize: 19, fontWeight: FontWeight.bold),
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text(
                          formattedDate,
                          maxLines: 1,
                          style: TextStyle(color: Colors.black, fontSize: 19),
                          overflow: TextOverflow.ellipsis,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ));
}

_cartItemListToProductResume(List<CartItem>? items){
  String productResume = "";
  items?.forEach((element) {
    productResume += productResume.isEmpty ? element.product?.name ?? "" : (", " + (element.product?.name ?? ""));
  });
  return productResume;
}