import 'package:flutter/material.dart';

import '../constants/app_colors.dart';

AppBar getEmporioAppBar(List<Widget> actions){
  return AppBar(
    backgroundColor: AppColors.green,
    title: const Image(
      image: AssetImage('assets/images/logo-text-only.png'),
    ),
    actions: actions,
  );
}