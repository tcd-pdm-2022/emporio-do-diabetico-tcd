import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const Color red = Color(0XFFFF3F3F);
  static const Color green = Color(0XFF39B44C);
  static const Color blue = Color(0XFF4BBDFF);
}