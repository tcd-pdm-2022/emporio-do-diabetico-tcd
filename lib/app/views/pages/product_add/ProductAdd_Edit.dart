import 'dart:io';

import 'package:emporio_do_diabetico/app/models/Product.dart';
import 'package:emporio_do_diabetico/app/repository/ProductController.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../shared/constants/app_colors.dart';
import '../../shared/constants/app_constants.dart';
import '../home_page/home.dart';

class ProductAdd_EditPage extends StatefulWidget {
  Product? product;
  ProductAdd_EditPage({Key? key, this.product}) : super(key: key);

  @override
  State<ProductAdd_EditPage> createState() => _ProductAdd_EditPageState();
}

class _ProductAdd_EditPageState extends State<ProductAdd_EditPage> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _priceController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  String _productImgPath = "";
  String _screenTitle = "Cadastrar Produto";
  String _conclusionMessage = "Produto cadastrado com sucesso!";
  ImagePicker _imgPicker = ImagePicker();
  final _productFormKey = GlobalKey<FormState>();
  late bool _isEdit;
  bool _imgWasEdited = false;
  bool _contentWasEdited = false;

  @override
  void initState() {
    super.initState();
    if(widget.product == null){
      _isEdit = false;
    }
    else{
      _isEdit = true;
      _screenTitle = "Editar Produto";
      _conclusionMessage = "Produto editado com sucesso!";
      _nameController.text = widget.product!.name!;
      _priceController.text = widget.product!.price!.toStringAsFixed(2);
      _descriptionController.text = widget.product!.description!;
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.green,
        title: const Image(
          image: AssetImage('assets/images/logo-text-only.png'),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          if(_productFormKey.currentState!.validate()){
            if(_isEdit == false){ //Validação, se for add
              if(_productImgPath == ""){
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text("Por favor, insira uma imagem!"),
                ));
              }
              else{
                Product newProduct = Product(
                  0,
                  _nameController.text,
                  double.parse(_priceController.text),
                  _descriptionController.text,
                  null,
                );
                newProduct.imagePATH = _productImgPath;
                addProduct(newProduct, context);
                _returnToHome();
              }
            }
            else{ //Validação, se for edit
              if(_imgWasEdited){
                widget.product!.imagePATH = _productImgPath;
              }

              if(_contentWasEdited){
                widget.product!.name = _nameController.text;
                widget.product!.price = double.parse(_priceController.text);
                widget.product!.description = _descriptionController.text;
              }

              if(_contentWasEdited || _imgWasEdited){
                updateProduct(widget.product!, context);
                _returnToHome();
              }
            }
          }
        },
        child: _isEdit ? const Icon(Icons.save, size: 40.0,) : const Icon(Icons.add, size: 40.0,),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Form(
          key: _productFormKey,
          child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    //Cabeçalho do Formulário ====================================
                    Text(
                      _screenTitle,
                      style: const TextStyle(
                        fontSize: 24.0,
                      ),
                    ),
                    const SizedBox(height: 10.0,),
                    //Seletor de Imagem ==========================================
                    GestureDetector(
                      onTap: (){
                        _imgPicker.pickImage(source: ImageSource.gallery).then((file){
                          if(file == null) return;
                          _imgWasEdited = true;
                          setState(() {
                            _productImgPath = file.path;
                          });
                        });
                      },
                      child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: AppColors.blue,
                            image: DecorationImage(
                                image: _getProductImage(),
                            ),
                          ),
                          alignment: Alignment.center,
                          height: 250.0,
                          child: (_productImgPath == "" && !_isEdit) ? const Icon(
                              Icons.add_a_photo_outlined,
                              color: Colors.white,
                              size: 75.0
                          ) : null
                      ),
                    ),
                    //Campo de Nome ==============================================
                    TextFormField(
                      controller: _nameController,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      decoration: const InputDecoration(
                        labelText: 'Nome do Produto *',
                      ),
                      onChanged: (text){
                        _contentWasEdited = true;
                      },
                      validator: (String? value) {
                        if(value == null || value.isEmpty){
                          return "Campo obrigatório!";
                        }
                        return null;
                      },
                    ),
                    //Campo do Preço ===========================================
                    TextFormField(
                      controller: _priceController,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      decoration: const InputDecoration(
                        labelText: 'Valor *',
                        prefixText: 'R\$ ',
                      ),
                      onChanged: (text){
                        _contentWasEdited = true;
                      },
                      validator: (String? value) {
                        if(value == null || value.isEmpty){
                          return "Campo obrigatório!";
                        }
                        return null;
                      },
                      keyboardType: TextInputType.number,
                    ),
                    //Campo da Descrição =======================================
                    TextFormField(
                      controller: _descriptionController,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      decoration: const InputDecoration(
                        labelText: 'Descrição *',
                      ),
                      onChanged: (text){
                        _contentWasEdited = true;
                      },
                      validator: (String? value) {
                        if(value == null || value.isEmpty){
                          return "Campo obrigatório!";
                        }
                        return null;
                      },
                      maxLines: null,
                    ),
                  ],
                ),
              )
          ),
        ),
      ),
    );
  }

  //Rotina de saída da tela
  void _returnToHome(){
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(_conclusionMessage),
        duration: const Duration(seconds: 3),
      ),
    );
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Home(currentIndex: 0))
    );
  }

  //Função que provê a imagem inicial que será exibida na interface de add/edit
  ImageProvider _getProductImage(){
    if(_isEdit && !_imgWasEdited){  //Se é edit, vamos pegar a imagem da URL
      return Image.network(
        widget.product!.imageURL ?? defaultimage,
        fit: BoxFit.cover,
        errorBuilder: (context, url, error) => Container(
          color: Colors.grey.withOpacity(0.5),
          child: const Icon(
            Icons.error,
            color: Colors.red,
            size: 50.0,
          ),
        ),
        frameBuilder: (context, child, frame, wasSynchronouslyLoaded) {
          return child;
        },
        loadingBuilder: (context, child, loadingProgress) {
          if (loadingProgress == null) return child;
          return Container(
            color: Colors.grey.withOpacity(0.25),
            child: const Center(child: CircularProgressIndicator()),
          );
        },
      ).image;
    }
    else{ //Senão, vamos usar o _productImgPath, que vai retornar vazio ou a imagem que foi selecionada pois é add
      return FileImage(File(_productImgPath));
    }
  }
}
