import 'package:emporio_do_diabetico/app/models/CartItem.dart';
import 'package:emporio_do_diabetico/app/models/Address.dart';
import 'package:emporio_do_diabetico/app/views/pages/checkout/PaymentInfo.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

import '../../../models/Order.dart';
import '../../shared/constants/app_colors.dart';

class DeliveryInfoPage extends StatefulWidget {
  Order order;

  DeliveryInfoPage({Key? key, required this.order}) : super(key: key);

  @override
  State<DeliveryInfoPage> createState() => _DeliveryInfoPageState();
}

class _DeliveryInfoPageState extends State<DeliveryInfoPage> {
  final _deliveryInfoFormKey = GlobalKey<FormState>();

  TextEditingController _addressController = TextEditingController();
  TextEditingController _numberController = TextEditingController();
  TextEditingController _complementController = TextEditingController();
  TextEditingController _cityController = TextEditingController();
  TextEditingController _stateController = TextEditingController();
  TextEditingController _postalcodeController = TextEditingController();

  final _cepInputMask = MaskTextInputFormatter(
    mask: "#####-###",
    filter: {"#": RegExp(r'[0-9]')},
    type: MaskAutoCompletionType.lazy
  );


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.green,
        title: const Image(
          image: AssetImage('assets/images/logo-text-only.png'),
        ),
        actions: const [
          IconButton(
            icon: Icon(null),
            onPressed: null,
          )
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: (){
          if(_deliveryInfoFormKey.currentState!.validate()){
            Address address = Address(
                _addressController.text,
                _numberController.text,
                _complementController.text,
                _cityController.text,
                _stateController.text,
                _postalcodeController.text
            );

            if(_complementController.text.isNotEmpty){
              address.complement = _complementController.text;
            }
            widget.order.address = address;

            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PaymentInfoPage(order: widget.order))
            );
          }
        },
        backgroundColor: AppColors.green,
        icon: Icon(
            Icons.credit_card,
            size: 40.0
        ),
        label: Text(
            "Informações de Pagamento",
            style: TextStyle(fontSize: 18.0),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: Padding(
        padding: EdgeInsets.all(10.0),
        child: SingleChildScrollView(
          child: Form(
            key: _deliveryInfoFormKey,
            child: Column(
              children: [
                //Cabeçalho do Formulário
                const Text(
                  "Informações para Entrega",
                  style: TextStyle(
                    fontSize: 24.0,
                  ),
                ),
                const SizedBox(height: 5.0,),
                const Text(
                  "Checkout 1/3",
                  style: TextStyle(
                      fontSize: 16.0,
                      color: Colors.grey
                  ),
                ),
                //Campo de Endereço ==============================================
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: _addressController,
                  decoration: const InputDecoration(
                    labelText: 'Endereço *',
                  ),
                  validator: (String? value) {
                    if(value == null || value.isEmpty){
                      return "Campo obrigatório!";
                    }
                    return null;
                  },
                  maxLines: null,
                ),
                //Campos de Número e Complemento =================================
                Row(
                  children: [
                    Flexible(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 5.0, 0),
                          child: TextFormField(
                            autovalidateMode: AutovalidateMode.onUserInteraction,
                            controller: _numberController,
                            decoration: const InputDecoration(
                              labelText: 'Número *',
                            ),
                            validator: (String? value) {
                              if(value == null || value.isEmpty){
                                return "Campo obrigatório!";
                              }
                              return null;
                            },
                            keyboardType: TextInputType.number,
                          ),
                        )
                    ),
                    Flexible(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(5.0, 0, 0, 0),
                          child: TextFormField(
                            controller: _complementController,
                            decoration: const InputDecoration(
                              labelText: 'Complemento',
                            ),
                            validator: (String? value) {
                              return null;
                            },
                          ),
                        )
                    ),
                  ],
                ),
                //Campo de Cidade ================================================
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: _cityController,
                  decoration: const InputDecoration(
                    labelText: 'Cidade *',
                  ),
                  validator: (String? value) {
                    if(value == null || value.isEmpty){
                      return "Campo obrigatório!";
                    }
                    return null;
                  },
                ),
                //Campo de Estado ================================================
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: _stateController,
                  decoration: const InputDecoration(
                    labelText: 'Estado *',
                  ),
                  validator: (String? value) {
                    if(value == null || value.isEmpty){
                      return "Campo obrigatório!";
                    }
                    return null;
                  },
                ),
                //Campo de CEP ===================================================
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: _postalcodeController,
                  decoration: const InputDecoration(
                    labelText: 'CEP *',
                  ),
                  inputFormatters: [_cepInputMask],
                  validator: (String? value) {
                    if(value == null || value.isEmpty){
                      return "Campo obrigatório!";
                    }
                    return null;
                  },
                  keyboardType: TextInputType.number,
                ),
              ],
            ),
          ),
        )
      )
    );
  }
}
