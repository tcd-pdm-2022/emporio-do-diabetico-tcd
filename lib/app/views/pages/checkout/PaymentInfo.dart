import 'package:emporio_do_diabetico/app/models/CreditCard.dart';
import 'package:emporio_do_diabetico/app/views/pages/checkout/OrderOverview.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

import '../../../models/CartItem.dart';
import '../../../models/Address.dart';
import '../../../models/Order.dart';
import '../../shared/constants/app_colors.dart';

class PaymentInfoPage extends StatefulWidget {
  Order order;
  PaymentInfoPage({Key? key, required this.order}) : super(key: key);

  @override
  State<PaymentInfoPage> createState() => _PaymentInfoPageState();
}

class _PaymentInfoPageState extends State<PaymentInfoPage> {
  late String dropdownValue;
  late List<String> paymentOptions;

  final _creditCardInfoFormKey = GlobalKey<FormState>();

  TextEditingController _cardNumberController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _cpfController = TextEditingController();
  TextEditingController _expirationDateController = TextEditingController();
  TextEditingController _cvvController = TextEditingController();

  final _cardNumberInputMask = MaskTextInputFormatter(
      mask: "####.####.####.####",
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy
  );

  final _cpfInputMask = MaskTextInputFormatter(
      mask: '###.###.###-##',
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy
  );

  final _expDateMask = MaskTextInputFormatter(
      mask: '##/##',
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy
  );

  final _cvvInputMask = MaskTextInputFormatter(
    mask: '###',
    filter: {"#": RegExp(r'[0-9]')},
    type: MaskAutoCompletionType.lazy
  );

  @override
  void initState() {
    paymentOptions = [
      "À vista R\$ ${widget.order.value!.toStringAsFixed(2)}",
      "2x de R\$ ${(widget.order.value!/2.0).toStringAsFixed(2)}",
      "3x de R\$ ${(widget.order.value!/3.0).toStringAsFixed(2)}",
      "6x de R\$ ${(widget.order.value!/6.0).toStringAsFixed(2)}"
    ];
    dropdownValue = paymentOptions[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.green,
          title: const Image(
            image: AssetImage('assets/images/logo-text-only.png'),
          ),
          actions: const [
            IconButton(
              icon: Icon(null),
              onPressed: null,
            )
          ],
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: (){
            if(_creditCardInfoFormKey.currentState!.validate()){
              CreditCard creditCard = CreditCard(
                  _cardNumberController.text,
                  _nameController.text,
                  _cpfController.text,
                  _expirationDateController.text,
                  _cvvController.text,
              );

              widget.order.creditCard = creditCard;
              widget.order.paymentOption = dropdownValue;

              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => OrderOverviewPage(order: widget.order)),
              );
            }
          },
          backgroundColor: AppColors.green,
          icon: const Icon(
              Icons.shopping_bag_outlined,
              size: 40.0
          ),
          label: const Text(
            "Resumo do Pedido",
            style: TextStyle(fontSize: 18.0),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: SingleChildScrollView(
            child: Form(
              key: _creditCardInfoFormKey,
              child: Column(
                children: [
                  //Cabeçalho do Formulário ======================================
                  const Text(
                    "Informações de Pagamento",
                    style: TextStyle(
                      fontSize: 24.0,
                    ),
                  ),
                  const SizedBox(height: 5.0,),
                  const Text(
                    "Checkout 2/3",
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.grey
                    ),
                  ),
                  //Campo de Número do Cartão ====================================
                  TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    inputFormatters: [_cardNumberInputMask],
                    controller: _cardNumberController,
                    decoration: const InputDecoration(
                      hintText: "****.****.****.****",
                      labelText: 'Número do Cartão *',
                    ),
                    validator: (String? value) {
                      if(value == null || value.isEmpty){
                        return "Campo obrigatório!";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.number,
                  ),
                  //Campo de Nome ================================================
                  TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    controller: _nameController,
                    decoration: const InputDecoration(
                      labelText: 'Nome (como escrito no cartão) *',
                    ),
                    validator: (String? value) {
                      if(value == null || value.isEmpty){
                        return "Campo obrigatório!";
                      }
                      return null;
                    },
                  ),
                  //Campo de CPF =================================================
                  TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    inputFormatters: [_cpfInputMask],
                    controller: _cpfController,
                    decoration: const InputDecoration(
                      hintText: "***.***.***-**",
                      labelText: 'CPF *',
                    ),
                    validator: (String? value) {
                      if(value == null || value.isEmpty){
                        return "Campo obrigatório!";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.number,
                  ),
                  //Campos de Número e Complemento =================================
                  Row(
                    children: [
                      Flexible(
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(0, 0, 5.0, 0),
                            child: TextFormField(
                              autovalidateMode: AutovalidateMode.onUserInteraction,
                              inputFormatters: [_expDateMask],
                              controller: _expirationDateController,
                              decoration: const InputDecoration(
                                hintText: "mm/aa",
                                labelText: 'Validade *',
                              ),
                              validator: (String? value) {
                                if(value == null || value.isEmpty){
                                  return "Campo obrigatório!";
                                }
                                return null;
                              },
                              keyboardType: TextInputType.number,
                            ),
                          )
                      ),
                      Flexible(
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(5.0, 0, 0, 0),
                            child: TextFormField(
                              autovalidateMode: AutovalidateMode.onUserInteraction,
                              inputFormatters: [_cvvInputMask],
                              controller: _cvvController,
                              decoration: const InputDecoration(
                                hintText: '***',
                                labelText: 'CVV *',
                              ),
                              validator: (String? value) {
                                if(value == null || value.isEmpty){
                                  return "Campo obrigatório!";
                                }
                                return null;
                              },
                              keyboardType: TextInputType.number,
                            ),
                          )
                      ),
                    ],
                  ),
                  //Campo de modo de pagamento ===================================
                  DropdownButtonFormField(
                    decoration: const InputDecoration(
                        labelText: "Forma de Pagamento"
                    ),
                    value: dropdownValue,
                    items: paymentOptions
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (String? newValue) {
                      setState(() {
                        dropdownValue = newValue!;
                      });
                    },
                  ),
                ],
              ),
            ),
          )
        )
    );
  }


}
