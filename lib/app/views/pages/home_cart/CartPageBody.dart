import 'package:emporio_do_diabetico/app/controllers/CartLocalStorageController.dart';
import 'package:emporio_do_diabetico/app/models/CartItem.dart';
import 'package:flutter/material.dart';
import '../../shared/components/cards/ProductCardCartPage.dart';
import '../../shared/constants/app_colors.dart';
import 'CartPageBottomResume.dart';

class CartPageBody extends StatefulWidget {
  List<CartItem> cartItems;
  CartPageBody({Key? key, required this.cartItems}) : super(key: key);

  @override
  State<CartPageBody> createState() => _CartPageBodyState();
}

class _CartPageBodyState extends State<CartPageBody> {
  @override
  Widget build(BuildContext context) {
    double subtotal = 0.0;
    widget.cartItems.forEach((item) {
      subtotal += (item.product!.price! * item.quantity);
    });

    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 12.0, bottom: 12.0),
          child: Container(
              child: Text(
                  widget.cartItems.length.toString() + " ITEMS NO CARRINHO",
                  textDirection: TextDirection.ltr,
                  style: const TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold))),
        ),
        Flexible(
          child: ListView.builder(
              itemCount: widget.cartItems.length, itemBuilder: _CartItemBuilder),
        ),
        bottomResume(context, subtotal, widget.cartItems)
      ],
    );
  }

  Widget _CartItemBuilder(context, index) {
    final item = widget.cartItems[index];
    return Dismissible(
      key: Key(UniqueKey().toString()),
      onDismissed: (direction) {
        if (direction == DismissDirection.startToEnd) {
          // Then show a snackbar.
          Scaffold.of(context).showSnackBar(SnackBar(
              content: Text("${item.product?.name} retirado"),
              duration: Duration(seconds: 3)));
          super.setState(() {
            item.quantity--;
            if (item.quantity == 0)
              widget.cartItems.remove(item);
            saveData(widget.cartItems);
          });
        } else {
          // Then show a snackbar.
          Scaffold.of(context).showSnackBar(SnackBar(
              content: Text("${item.product?.name} adicionado"),
              duration: Duration(seconds: 3)));
          super.setState(() {
            item.quantity++;
            saveData(widget.cartItems);
          });
        }
      },
      // Show a red background as the item is swiped away.
      background: Container(
        decoration: BoxDecoration(color: Colors.red),
        padding: EdgeInsets.all(5.0),
        child: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 20.0),
              child: item.quantity == 1
                  ? const Icon(Icons.delete, color: Colors.white)
                  : const Icon(Icons.exposure_minus_1, color: Colors.white),
            ),
          ],
        ),
      ),
      secondaryBackground: Container(
        decoration: BoxDecoration(color: AppColors.green),
        padding: EdgeInsets.all(5.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: const <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: Icon(Icons.plus_one, color: Colors.white),
            ),
          ],
        ),
      ),
      child: ProductCard(context, item),
    );
  }
}
