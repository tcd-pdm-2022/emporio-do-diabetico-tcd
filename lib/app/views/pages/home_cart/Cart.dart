import 'package:emporio_do_diabetico/app/views/pages/checkout/DeliveryInfo.dart';
import 'package:emporio_do_diabetico/app/controllers/CartLocalStorageController.dart';
import 'package:emporio_do_diabetico/app/views/shared/constants/app_colors.dart';
import 'package:flutter/material.dart';

import '../../../models/CartItem.dart';
import '../../../models/Product.dart';
import '../../../repository/ProductController.dart';
import '../../shared/components/FutureOrStremBuilderHelper.dart';
import 'CartPageBody.dart';

class CartPage extends StatefulWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  State<CartPage> createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _getProducts(),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return containerWithCircularProgress();
          } else if (snapshot.hasError) {
            return containerWithErrorMessage(snapshot.error.toString());
          } else if (snapshot.hasData) {

            return CartPageBody(cartItems: snapshot.data,);
          } else {
            return containerWithErrorMessage("Erro :(");
          }
        });
  }

  Future<List<CartItem>> _getProducts () async {
    List<CartItem> cartItems = [];
    List<CartItem> localStorage = await getCartItems() ?? [];
    List<Product> products = await getProductsFromDB();
    for (var element in localStorage) {
      Iterable<Product> productInfo = products.where((element2) => element2.id == element.productId);
      if(productInfo.isNotEmpty && productInfo.first != null)
        cartItems.add(CartItem(element.productId, element.quantity,productInfo.first));
    }

    return cartItems;
  }
}




