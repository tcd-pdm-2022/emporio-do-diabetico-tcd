import 'package:emporio_do_diabetico/app/repository/ProductController.dart';
import 'package:emporio_do_diabetico/app/views/pages/checkout/DeliveryInfo.dart';
import 'package:flutter/material.dart';
import '../../../controllers/UserSingleton.dart';
import '../../../models/CartItem.dart';
import '../../../models/Order.dart';
import '../../../models/Product.dart';
import '../../shared/constants/app_colors.dart';
import '../../shared/constants/app_constants.dart';

Widget bottomResume(context, double subtotal, List<CartItem> cartItems) {
  var deliveryCost = subtotal == 0 ? 0 : shippingCost;
  return Column(
    children: [
      const Padding(
        padding: EdgeInsets.only(bottom: 10.0),
        child: Text("Deslize para alterar a quantidade"),
      ),
      Container(
          child: Padding(
        padding: const EdgeInsets.only(left: 20.0, right: 20),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 18.0),
              child: Row(
                children: [
                  const Expanded(
                      child: Text(
                    "TOTAL",
                    style: TextStyle(
                        fontSize: 18,
                        color: AppColors.green,
                        fontWeight: FontWeight.bold),
                  )),
                  Text("R\$ ${(subtotal + deliveryCost).toStringAsFixed(2)}",
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: AppColors.green,
                      )),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Row(
                children: [
                  const Expanded(
                      child: Text("Subtotal", style: TextStyle(fontSize: 14))),
                  Text("R\$ ${subtotal.toStringAsFixed(2)}",
                      style: TextStyle(fontSize: 14, color: Colors.grey)),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Row(
                children: [
                  const Expanded(
                      child: Text("Frete", style: TextStyle(fontSize: 14))),
                  Text("R\$ ${deliveryCost.toStringAsFixed(2)}",
                      style: TextStyle(fontSize: 14, color: Colors.grey)),
                ],
              ),
            ),
          ],
        ),
      )),
      Padding(
        padding:
            const EdgeInsets.only(left: 20.0, right: 20, top: 10, bottom: 10),
        //TODO: CHANGE BUTTON STYLE WITH BORDER RADIUS TO MATCH OTHER PAGE BUTTONS
        child: ButtonTheme(
          buttonColor: Theme.of(context).primaryColor,
          minWidth: double.infinity,
          height: 40.0,
          child: RaisedButton(

            disabledColor: Theme.of(context).primaryColor.withOpacity(0.7),
            onPressed: subtotal == 0 ?  null : () async {
              if(SingletonUser().user != null && SingletonUser().user!.id != null){
                //TODO: GO TO CHECKOUT PAGE - LEVARVARIÁVEL "cartItems" PARA REGISTRAR O PEDIDO
                Order order = Order(
                  null,             //id
                  SingletonUser().user!.id,
                  SingletonUser().user,//cliente
                  subtotal,         //value
                  null,             //address
                  null,             //creditcard
                  cartItems,        //cartItems
                  null,             //paymentOption
                  null,
                );
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => DeliveryInfoPage(order: order))
                );
              }
            },
            child: const Text(
              "Finalizar Compra",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ),
        ),
      ),
    ],
  );
}
