import "package:flutter/material.dart";

import '../../../controllers/UserSingleton.dart';
import '../../../models/User.dart';
import '../../../repository/UserController.dart';
import '../../shared/constants/app_colors.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    Future.delayed(Duration(seconds: 3)).then((_) async {
      bool skipLogin = false;
      if(skipLogin){
        User? user = await login("joao@gmail.com", "12345");
        SingletonUser().user = user;
        Navigator.pushReplacementNamed(context, "/home");
      } else {
        Navigator.pushReplacementNamed(context, '/login');
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      color: AppColors.green,
      child: Center(child: Image.asset(
        "assets/images/logo-splash.png",
        width: size.width*0.50,
      ),),
    );
  }
}
