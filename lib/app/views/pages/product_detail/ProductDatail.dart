import 'package:emporio_do_diabetico/app/controllers/CartLocalStorageController.dart';
import 'package:emporio_do_diabetico/app/repository/ProductController.dart';
import 'package:emporio_do_diabetico/app/views/pages/product_add/ProductAdd_Edit.dart';
import 'package:flutter/material.dart';
import '../../../controllers/UserSingleton.dart';
import '../../../models/CartItem.dart';
import '../../../models/Product.dart';
import '../../shared/components/EmporioAppBar.dart';
import '../../shared/components/cards/ProductImage.dart';
import '../../shared/constants/app_colors.dart';

class ProductDetail extends StatelessWidget {
  Product product;
  ProductDetail({Key? key, required this.product}) : super(key: key);
  bool _adminView = false;

  void checkAdminView() {
    if(SingletonUser().user != null){
      if(SingletonUser().user!.isAdmin != null && SingletonUser().user!.isAdmin == true){
        _adminView = true;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    checkAdminView();
    return Scaffold(
      appBar: getEmporioAppBar([]),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: (MediaQuery.of(context).size.width / 1.5),
              width: double.infinity,
              child: ProductImage(
                product: product,
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 0),
                  child: Text(
                    product.name ?? "",
                    textAlign: TextAlign.start,
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15.0, 20.0, 15.0, 0),
                  child: Text('R\$ ${product.price?.toStringAsFixed(2) ?? ""}',
                      textAlign: TextAlign.left,
                      style: const TextStyle(
                        color: AppColors.green,
                        fontSize: 25,
                        fontWeight: FontWeight.w600,
                      )),
                ),
                const Padding(
                  padding: EdgeInsets.fromLTRB(15.0, 20.0, 15.0, 5.0),
                  child: Text(
                    "Descrição do Produto:",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 22,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 30.0),
                  child: Text(
                    product.description ??
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ac placerat orci. In nulla arcu, dapibus ut nisl commodo, cursus imperdiet ante. Aenean mattis convallis euismod. Morbi quis maximus turpis. Vivamus eu urna turpis. Donec egestas nunc pretium pulvinar congue. Maecenas leo metus, vulputate ac eleifend rutrum, ultrices dignissim velit.",
                    //product.description ?? "",
                    textAlign: TextAlign.start,
                    style: const TextStyle(
                      color: Colors.black54,
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: AppColors.green,
                      ),
                      onPressed: () {
                        CartItem item = CartItem(product.id!, 1);
                        addItemToCart(item);
                        Navigator.pop(context);
                      },
                      child: Container(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Icon(Icons.shopping_cart, size: 40.0),
                            SizedBox(width: 10.0,),
                            Text(
                              "Adicionar ao Carrinho",
                              style: TextStyle(fontSize: 20.0),
                            ),
                          ],
                        ),
                      )
                  ),
                  _adminView == true ? Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: AppColors.blue,
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProductAdd_EditPage(product: product,))
                            );
                          },
                          child: Container(
                            padding: const EdgeInsets.all(10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const [
                                Icon(Icons.edit, size: 40.0),
                                SizedBox(width: 10.0,),
                                Text(
                                  "Editar Produto",
                                  style: TextStyle(fontSize: 20.0),
                                ),
                              ],
                            ),
                          )
                      ),
                  ) : const SizedBox.shrink(),
                  _adminView == true ? Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: AppColors.red,
                        ),
                        onPressed: () async {
                          await deleteProduct(product.id!, context);
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              content: Text("Produto removido!"),
                              duration: Duration(seconds: 3),
                            ),
                          );
                          Navigator.pop(context);
                        },
                        child: Container(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const [
                              Icon(Icons.delete, size: 40.0),
                              SizedBox(width: 10.0,),
                              Text(
                                "Remover Produto",
                                style: TextStyle(fontSize: 20.0),
                              ),
                            ],
                          ),
                        )
                    ),
                  ) : const SizedBox.shrink(),
                ],
              )
            ),
          ],
        ),
      ),
    );
  }
}
