import 'package:emporio_do_diabetico/app/views/pages/login_register_page/register.dart';
import 'package:emporio_do_diabetico/app/views/shared/components/MostarToast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../../controllers/UserSingleton.dart';
import '../../../models/User.dart';
import '../../../repository/UserController.dart';
import '../../shared/constants/app_colors.dart';
import '../../shared/constants/app_constants.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _inputEmail = TextEditingController();
  TextEditingController _inputPassword = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, bottom: 20),
            child: CircleAvatar(
              backgroundImage: Image(image: AssetImage('assets/images/logo-login.png')).image,
              radius: 117,
              backgroundColor: Colors.grey,
            ),
          ),
          Container(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 15),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        keyboardType: TextInputType.emailAddress,
                        decoration: const InputDecoration(
                            hintText: "ex: zezinhoDaSilva@email.com",
                            labelText: "Digite seu e-mail:",
                            labelStyle: TextStyle(
                                fontSize: 20,
                                color: AppColors.green,
                                fontWeight: FontWeight.w500)),
                        style: const TextStyle(
                            fontSize: 24,
                            color: Colors.black87,
                            fontWeight: FontWeight.w400),
                        controller: _inputEmail,
                        validator: (String? value) {
                          if (value == null ||
                              value.isEmpty ||
                              !value.contains("@") ||
                              !value.contains(".")) {
                            return "Valor inválido!";
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        keyboardType: TextInputType.text,
                        decoration: const InputDecoration(
                            hintText: "ex: *********",
                            labelText: "Digite sua senha:",
                            labelStyle: TextStyle(
                                fontSize: 20,
                                color: AppColors.green,
                                fontWeight: FontWeight.w500)),
                        style: const TextStyle(
                            fontSize: 24,
                            color: Colors.black87,
                            fontWeight: FontWeight.w400),
                        obscureText: true,
                        controller: _inputPassword,
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return "Campo obrigatório!";
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 30.0, bottom: 10.0),
                      child: RaisedButton.icon(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            User? user = await login(_inputEmail.text, _inputPassword.text);
                            if(user!=null) {
                              SingletonUser().user = user;
                              Navigator.pushReplacementNamed(context, "/home");
                            } else {
                              MostrarToast("Usuario e/ou senha não foram localizados!");
                            }
                          } else {
                            MostrarToast("Preencha os campos obrigatórios para entrar!");
                          }
                        },
                        color: AppColors.green,
                        textColor: Colors.white,
                        padding: const EdgeInsets.all(15.0),
                        icon: const Icon(Icons.account_box, size: 26.0),
                        label: const Text(
                          "Entrar",
                          style: TextStyle(fontSize: 20.0),
                        ),
                      ),
                    ),
                  ],
                ),
              ))
        ],
      ),
    );
  }
}
