import 'package:emporio_do_diabetico/app/repository/UserController.dart';
import 'package:emporio_do_diabetico/app/views/pages/login_register_page/login.dart';
import 'package:emporio_do_diabetico/app/views/shared/components/EmporioAppBar.dart';
import 'package:emporio_do_diabetico/app/views/shared/constants/app_constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

import '../../../controllers/UserSingleton.dart';
import '../../../models/User.dart';
import '../../shared/components/MostarToast.dart';
import '../../shared/constants/app_colors.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController _inputName = TextEditingController();
  TextEditingController _inputCpf = TextEditingController();
  final _cpfInputMask = MaskTextInputFormatter(
      mask: '###.###.###-##',
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy);

  TextEditingController _inputPhoneNumber = TextEditingController();
  final _phoneNumberInputMask = MaskTextInputFormatter(
      mask: '(##) # ####-####',
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy);

  TextEditingController _inputEmail = TextEditingController();
  TextEditingController _inputPassword = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, bottom: 20),
            child: CircleAvatar(
              backgroundImage: Image(image: AssetImage('assets/images/logo-login.png')).image,
              radius: 117,
              backgroundColor: Colors.grey,
            ),
          ),
          Container(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 15),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    TextFormField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      keyboardType: TextInputType.name,
                      decoration: const InputDecoration(
                          hintText: "Ex: Zezinho Cleiton da Silva",
                          labelText: "Digite seu nome:",
                          labelStyle: TextStyle(
                              fontSize: 20,
                              color: AppColors.green,
                              fontWeight: FontWeight.w500)),
                      style: const TextStyle(
                          fontSize: 24,
                          color: Colors.black87,
                          fontWeight: FontWeight.w400),
                      controller: _inputName,
                      validator: (String? value) {
                        if (value == null || value.isEmpty) {
                          return "Campo obrigatório!";
                        }
                        return null;
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          keyboardType: TextInputType.number,
                          decoration: const InputDecoration(
                              hintText: "ex: 123.456.789-00",
                              labelText: "Digite seu cpf:",
                              labelStyle: TextStyle(
                                  fontSize: 20,
                                  color: AppColors.green,
                                  fontWeight: FontWeight.w500)),
                          style: const TextStyle(
                              fontSize: 24,
                              color: Colors.black87,
                              fontWeight: FontWeight.w400),
                          inputFormatters: [_cpfInputMask],
                          controller: _inputCpf,
                          validator: (String? value) {
                            if (value == null || value.isEmpty || value.length != 14) {
                              return "Campo obrigatório!";
                            }
                            return null;
                          }),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          keyboardType: TextInputType.phone,
                          decoration: const InputDecoration(
                              hintText: "ex: (99) 9 9999-9999",
                              labelText: "Digite seu número de telefone:",
                              labelStyle: TextStyle(
                                  fontSize: 20,
                                  color: AppColors.green,
                                  fontWeight: FontWeight.w500)),
                          style: const TextStyle(
                              fontSize: 24,
                              color: Colors.black87,
                              fontWeight: FontWeight.w400),
                          inputFormatters: [_phoneNumberInputMask],
                          controller: _inputPhoneNumber,
                          validator: (String? value) {
                            if (value == null || value.isEmpty || value.length != 16) {
                              return "Campo obrigatório!";
                            }
                            return null;
                          }),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          keyboardType: TextInputType.emailAddress,
                          decoration: const InputDecoration(
                              hintText: "ex: zezinhoDaSilva@email.com",
                              labelText: "Digite seu e-mail:",
                              labelStyle: TextStyle(
                                  fontSize: 20,
                                  color: AppColors.green,
                                  fontWeight: FontWeight.w500)),
                          style: const TextStyle(
                              fontSize: 24,
                              color: Colors.black87,
                              fontWeight: FontWeight.w400),
                          controller: _inputEmail,
                          validator: (String? value) {
                            if (value == null || value.isEmpty || !value.contains("@") || !value.contains(".")) {
                              return "Valor inválido!";
                            }
                            return null;
                          }),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: TextFormField(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          keyboardType: TextInputType.text,
                          decoration: const InputDecoration(
                              hintText: "ex: *********",
                              labelText: "Digite sua senha:",
                              labelStyle: TextStyle(
                                  fontSize: 20,
                                  color: AppColors.green,
                                  fontWeight: FontWeight.w500)),
                          style: const TextStyle(
                              fontSize: 24,
                              color: Colors.black87,
                              fontWeight: FontWeight.w400),
                          obscureText: true,
                          controller: _inputPassword,
                          validator: (String? value) {
                            if (value == null || value.isEmpty) {
                              return "Campo obrigatório!";
                            }
                            return null;
                          }),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0, bottom: 10.0),
                      child: RaisedButton.icon(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            var newUser = User(
                                0,
                                _inputName.text,
                                _inputEmail.text,
                                _inputCpf.text,
                                _inputPhoneNumber.text,
                                _inputPassword.text,
                                DateTime.now(),
                                false);
                            await addUser(newUser, context);
                            User? user = await login(newUser.email!, newUser.password!);

                            if(user!=null) {
                              SingletonUser().user = user;
                              Navigator.pushReplacementNamed(context, "/home");
                            }
                          } else {
                            MostrarToast("Preencha os campos obrigatórios para se registrar!");
                          }
                        },
                        color: AppColors.green,
                        textColor: Colors.white,
                        padding: const EdgeInsets.all(15.0),
                        icon: const Icon(Icons.supervisor_account, size: 28.0),
                        label: const Text(
                          "Registrar-se",
                          style: TextStyle(fontSize: 22.0),
                        ),
                      ),
                    ),
                  ],
                ),
              ))
        ],
      ),
    );
  }
}
