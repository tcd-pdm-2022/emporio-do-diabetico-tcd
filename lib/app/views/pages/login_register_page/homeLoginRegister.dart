

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../shared/components/EmporioAppBar.dart';
import '../../shared/constants/app_colors.dart';
import 'login.dart';
import 'register.dart';

class HomeLoginRegister extends StatefulWidget {
  @required
  int currentIndex;
  final List<Widget> tabs = [ LoginPage(), RegisterPage()];

  HomeLoginRegister({Key? key, required this.currentIndex}) : super(key: key);

  @override
  State<HomeLoginRegister> createState() => _HomeLoginRegisterState();
}

class _HomeLoginRegisterState extends State<HomeLoginRegister> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: getEmporioAppBar([]),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: AppColors.green,
          selectedItemColor: Colors.white,
          items: const [
            BottomNavigationBarItem(icon: Icon(Icons.account_box),label: "Login"),
            BottomNavigationBarItem(icon: Icon(Icons.supervisor_account_sharp), label: "Criar Conta"),
          ],
          onTap: (index){
            setState(() {
              widget.currentIndex = index;
            });
          },
          currentIndex: widget.currentIndex,
        ),
        body: SafeArea(
          child: widget.tabs[widget.currentIndex],)
    );
  }
}
