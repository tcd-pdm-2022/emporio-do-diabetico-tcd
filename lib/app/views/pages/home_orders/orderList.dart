import 'package:flutter/material.dart';
import '../../../models/Order.dart';
import '../../../models/Product.dart';
import '../../shared/components/cards/OrderListCard.dart';
import '../../shared/components/cards/ProductCardHomeGridView.dart';

class OrderList extends StatefulWidget {
  List<Order> orders;
  OrderList({Key? key, required this.orders}) : super(key: key);

  @override
  State<OrderList> createState() => _OrderListState();
}

class _OrderListState extends State<OrderList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: widget.orders.length,
        padding: const EdgeInsets.only(top: 8, left: 6, right: 6, bottom: 12),
        itemBuilder: (context, index) {
          Order orderToShow = widget.orders[index];
          return OrderListCard(context, orderToShow);
      },)
    );
  }
}
