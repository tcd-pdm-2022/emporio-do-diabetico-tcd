import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:emporio_do_diabetico/app/views/shared/constants/app_colors.dart';
import 'package:flutter/material.dart';

import '../../../controllers/UserSingleton.dart';
import '../../../models/Order.dart';
import '../../../models/User.dart';
import '../../shared/components/FutureOrStremBuilderHelper.dart';
import 'orderList.dart';

class OrdersPage extends StatefulWidget {
  const OrdersPage({Key? key}) : super(key: key);

  @override
  State<OrdersPage> createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrdersPage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: FirebaseFirestore.instance
            .collection("orders")
            .where("clientId", isEqualTo: SingletonUser().user?.id ?? 0)
            .where("visible", isEqualTo: true)
            .orderBy("orderDate")
            .get(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return containerWithCircularProgress();
          } else if (snapshot.hasError) {
            return containerWithErrorMessage(snapshot.error.toString());
          } else if (snapshot.hasData) {
            List<Order> orders = [];
            for (var element in snapshot.data?.docs ?? []) {
              orders.add(docToOrder(element));
            }

            return orders.isNotEmpty ? OrderList(orders: orders)
                : containerWithInfoMessage("Você ainda não realizou um pedido! Veja nossos produtos e conheça a experiência de comprar no Empório do Diabético!!");
          } else {
            return containerWithErrorMessage("Erro :(");
          }
        });
  }
}
