import 'dart:ui';

import 'package:emporio_do_diabetico/app/repository/OrderController.dart';
import 'package:emporio_do_diabetico/app/views/pages/home_page/home.dart';
import 'package:flutter/material.dart';

import '../../../models/Order.dart';
import '../../shared/components/cards/ProductCardCartPage.dart';
import '../../shared/constants/app_colors.dart';

class OrderDetails extends StatefulWidget {
  Order order;
  OrderDetails({Key? key, required this.order}) : super(key: key);

  @override
  State<OrderDetails> createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.green,
          title: const Image(
            image: AssetImage('assets/images/logo-text-only.png'),
          ),
          actions: const [
            IconButton(
              icon: Icon(null),
              onPressed: null,
            )
          ],
        ),
        body: Padding(
            padding: EdgeInsets.all(10.0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  //Cabeçalho da Tela ==============================================
                  Container(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Text(
                          "Resumo do Pedido",
                          style: TextStyle(
                            fontSize: 24.0,
                          ),
                        ),
                        SizedBox(height: 5.0,)
                      ],
                    ),
                  ),
                  //Corpo da Tela ==================================================
                  //Campo da lista de produtos =====================================
                  Container(
                    decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(color: Colors.black))
                    ),
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 10.0, 0, 0),
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 5.0),
                      height: 300.0,
                      child: ListView.builder(
                          itemCount: widget.order.cartItems!.length, itemBuilder: _cartItemListBuilder
                      ),
                    ),
                  ),
                  //Informações de Entrega =========================================
                  Container(
                      decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(color: Colors.black))
                      ),
                      padding: EdgeInsets.fromLTRB(0, 5.0, 0, 5.0),
                      alignment: Alignment.center,
                      child: Row(
                        children: [
                          Container(
                            padding: EdgeInsets.all(10.0),
                            child: Icon(
                              Icons.place,
                              size: 40.0,
                              color: AppColors.green,
                            ),
                          ),
                          Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Endereço para entrega",
                                    style: TextStyle(
                                        fontSize: 22.0,
                                        fontWeight: FontWeight.bold,
                                      color: AppColors.green
                                    ),
                                  ),
                                  SizedBox(height: 5.0),
                                  Text(
                                    "${widget.order.address!.address}, ${widget.order.address!.number} ${widget.order.address!.complement}",
                                    style: TextStyle(
                                      fontSize: 18.0,
                                    ),
                                  ),
                                  Text(
                                    "${widget.order.address!.city} - ${widget.order.address!.state}, Brasil",
                                    style: TextStyle(
                                      fontSize: 18.0,
                                    ),
                                  ),
                                  Text(
                                    "CEP: ${widget.order.address!.postal_code}",
                                    style: TextStyle(
                                      fontSize: 18.0,
                                    ),
                                  ),
                                ],
                              )
                          ),
                        ],
                      )
                  ),
                  //Informações de Pagamento =======================================
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 5.0, 0, 5.0),
                    alignment: Alignment.center,
                    child: Row(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10.0),
                          child: Icon(
                            Icons.credit_card,
                            size: 40.0,
                            color: AppColors.green,
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Informações de Pagamento",
                              style: TextStyle(
                                  fontSize: 22.0,
                                  fontWeight: FontWeight.bold,
                                color: AppColors.green
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(0, 10.0, 0, 0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Cartão: ${widget.order.creditCard!.cardNumber.replaceRange(0, widget.order.creditCard!.cardNumber.length - 4, "**** **** **** ")}",
                                    style: TextStyle(
                                      fontSize: 18.0,
                                    ),
                                  ),
                                  Text(
                                    "${widget.order.paymentOption}",
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
        )
    );
  }

  //Card de item do carrinho ===================================================
  Widget _cartItemListBuilder(context, index){
    return ProductCard(context, widget.order.cartItems![index]);
  }

  //Rotina de saída da tela
  void _returnToHome(){
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text("Pedido realizado com sucesso!"),
        duration: Duration(seconds: 3),
      ),
    );
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Home(currentIndex: 0))
    );
  }
}
