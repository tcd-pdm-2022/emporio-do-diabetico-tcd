import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:emporio_do_diabetico/app/views/pages/home_products_page/productGrid.dart';
import 'package:emporio_do_diabetico/app/views/shared/constants/app_colors.dart';
import 'package:emporio_do_diabetico/app/views/shared/constants/app_constants.dart';
import 'package:flutter/material.dart';
import '../../../models/Product.dart';
import '../../../repository/ProductController.dart';
import '../../shared/components/FutureOrStremBuilderHelper.dart';

class ProductsPage extends StatefulWidget {
  const ProductsPage({Key? key}) : super(key: key);

  @override
  State<ProductsPage> createState() => _ProductsPageState();
}

class _ProductsPageState extends State<ProductsPage> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: getProductStreamFirebaseSnapshot,
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return containerWithCircularProgress();
          } else if (snapshot.hasError) {
            return containerWithErrorMessage(snapshot.error.toString());
          } else if (snapshot.hasData) {
            List<Product> products = [];
            for (var element in snapshot.data?.docs ?? []) {
              products.add(docToProduct(element));
            }
            return ProductGrid(products: products);
          } else {
            return containerWithErrorMessage("Erro :(");
          }
        });
  }
}
