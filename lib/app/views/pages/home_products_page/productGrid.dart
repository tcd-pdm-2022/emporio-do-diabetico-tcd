import 'package:flutter/material.dart';
import '../../../models/Product.dart';
import '../../shared/components/cards/ProductCardHomeGridView.dart';

class ProductGrid extends StatefulWidget {
  List<Product> products;
  ProductGrid({Key? key, required this.products}) : super(key: key);

  @override
  State<ProductGrid> createState() => _ProductGridState();
}

class _ProductGridState extends State<ProductGrid> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView.count(
        shrinkWrap: false,
        crossAxisCount: 2,
        childAspectRatio: 0.7,
        padding: const EdgeInsets.only(top: 8, left: 6, right: 6, bottom: 12),
        children: List.generate(widget.products.length, (index) {
          Product productToShow = widget.products[index];
          return ProductCardHomeGridView(product: productToShow);
        }),
      ),
    );
  }
}
