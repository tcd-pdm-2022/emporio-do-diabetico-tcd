import 'package:emporio_do_diabetico/app/controllers/UserSingleton.dart';
import 'package:emporio_do_diabetico/app/views/pages/login_register_page/login.dart';
import 'package:flutter/material.dart';

import '../../../models/User.dart';
import '../product_add/ProductAdd_Edit.dart';
import '../login_register_page/homeLoginRegister.dart';

class AppDrawer extends StatefulWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  State<AppDrawer> createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  bool _adminView = false;
  @override
  void initState() {
    if(SingletonUser().user != null){
      if(SingletonUser().user!.isAdmin != null && SingletonUser().user!.isAdmin == true){
        _adminView = true;
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    User? user = SingletonUser().user;
    var name = user?.name ?? "ADM Empório";
    var email = user?.email ?? "adm@emporiodiabetico.com";
    return Column(
      children: <Widget>[
        UserAccountsDrawerHeader(
          decoration: const BoxDecoration(
              image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage('assets/images/background.jpg'),
          )),
          currentAccountPicture: const CircleAvatar(
            backgroundImage: AssetImage('assets/images/profile.png'),
          ),
          accountEmail: Text(email),
          accountName: Text(name),
        ),
        Expanded(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              ListTile(
                leading:
                    Icon(Icons.home, color: Theme.of(context).primaryColor),
                title: Text('Home'),
                onTap: () {
                  Navigator.pushReplacementNamed(context, "/home");
                },
              ),
              _adminView == true ? ListTile(
                leading: Icon(Icons.supervisor_account_sharp,
                    color: Theme.of(context).primaryColor),
                title: Text('Cadastrar Usuário'),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => HomeLoginRegister(currentIndex: 1))
                  );
                },
              ) : const SizedBox.shrink(),
              _adminView == true ? ListTile(
                leading: Icon(Icons.add_box_outlined, color: Theme.of(context).primaryColor),
                title: Text('Cadastrar Produto'),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ProductAdd_EditPage())
                  );
                }
              ) : const SizedBox.shrink(),
              Divider(),
              ListTile(
                leading: Icon(Icons.exit_to_app,
                    color: Theme.of(context).primaryColor),
                title: Text('Sair'),
                onTap: () {
                  SingletonUser().user = null;
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => HomeLoginRegister(currentIndex: 0,))
                  );

                },
              ),
            ],
          ),
        )
      ],
    );
  }
}
