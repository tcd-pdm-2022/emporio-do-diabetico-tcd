import 'package:emporio_do_diabetico/app/controllers/CartLocalStorageController.dart';
import 'package:emporio_do_diabetico/app/views/pages/home_cart/Cart.dart';
import 'package:emporio_do_diabetico/app/views/pages/home_orders/Orders.dart';
import 'package:emporio_do_diabetico/app/views/pages/home_products_page/products_page.dart';
import 'package:emporio_do_diabetico/app/views/shared/components/EmporioAppBar.dart';
import 'package:emporio_do_diabetico/app/views/shared/constants/app_colors.dart';
import 'package:flutter/material.dart';

import 'drawer.dart';



class Home extends StatefulWidget {
  @required
  int currentIndex;
  final List<Widget> tabs = [ ProductsPage(), OrdersPage(), CartPage() ];
  Home({Key? key, required this.currentIndex}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getEmporioAppBar([
        //TODO: Add Product Button when ADM logged User
        IconButton(
          icon: widget.currentIndex == 2 ? Icon(Icons.delete, color: Colors.white,) : Icon(null),
          onPressed: widget.currentIndex == 2 ? onCleanCartTap : null,
        )
      ]),
      drawer: const Drawer(
        child: AppDrawer(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: AppColors.green,
        selectedItemColor: Colors.white,
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.store),label: "Produtos"),
          BottomNavigationBarItem(icon: Icon(Icons.list_alt), label: "Pedidos"),
          BottomNavigationBarItem(icon: Icon(Icons.shopping_cart), label: "Carrinho"),
        ],
        onTap: (index){
          setState(() {
            widget.currentIndex = index;
          });
        },
        currentIndex: widget.currentIndex,
      ),
      body: SafeArea(
        child: widget.tabs[widget.currentIndex],)
    );
  }

  onCleanCartTap() async {
    await cleanCart();
    setState(() {
      widget.tabs[2] = CartPage();
    });
  }
}

