/*class Order {
  int? id;
  int? clientId;
  String? clientName;
  double? value;
  Address? address;
  CreditCard? creditCard;
  List<CartItem>? cartItems;
  String? paymentOption;
  DateTime? orderDate;
}*/

import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:emporio_do_diabetico/app/repository/Shared.dart';
import 'package:flutter/material.dart';

import '../models/Order.dart';

Future<void> addOrder(Order newOrder, BuildContext context) async{
  int id = await whoIsNextCollectionId("orders");
  return await FirebaseFirestore.instance.collection("orders").doc(id.toString()).set({
    "id": id,
    "clientId": newOrder.clientId,
    "cliente": jsonEncode(newOrder.cliente),
    "value": newOrder.value,
    "address": jsonEncode(newOrder.address),
    "creditCard": jsonEncode(newOrder.creditCard),
    "cartItems": jsonEncode(newOrder.cartItems),
    "paymentOption": newOrder.paymentOption,
    "orderDate": newOrder.orderDate,
    "visible": true,
  });
}

Future<void> updateOrder(Order order, BuildContext context) async{
  if(order.id == null) return;
  return await FirebaseFirestore.instance
      .collection("orders")
      .doc(order.id.toString())
      .update({
        "value": order.value,
        "address": order.address,
        "creditCard": order.creditCard,
        "cartItems": order.cartItems,
        "paymentOption": order.paymentOption,
      });
}

Future<void> deleteOrder(int id, BuildContext context) async{
  return await FirebaseFirestore.instance
      .collection("orders")
      .doc(id.toString())
      .update({
      "visible": false,
  });
}

Future<List<Order>> getOrdersFromDB() async {
  List<Order> orders = [];
  await FirebaseFirestore.instance
  .collection("orders")
  .where("visible", isEqualTo: true)
  .orderBy("orderDate")
  .get().then((QuerySnapshot querySnapshot){
    for(var element in querySnapshot.docs){
      orders.add(docToOrder(element));
    }
  });
  return orders;
}

var getOrderFutureFirebaseSnapshot = FirebaseFirestore.instance
    .collection("orders")
    .where("visible", isEqualTo: true)
    .orderBy("orderDate")
    .get();