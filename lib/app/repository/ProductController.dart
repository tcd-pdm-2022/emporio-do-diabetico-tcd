import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:emporio_do_diabetico/app/repository/Shared.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart';
import '../models/Product.dart';

/*
  class Product {
    int? id;
    String? name;
    double? price;
    String? description;
    String? imageURL;
  }
*/

Future<void> addProduct(Product newProduct, BuildContext context) async {
  int id = await whoIsNextCollectionId("products");
  newProduct.imageURL = await uploadImage(newProduct.imagePATH!);
  return await FirebaseFirestore.instance.collection("products").doc(id.toString()).set({
    "id": id,
    "name": newProduct.name,
    "price": newProduct.price,
    "description": newProduct.description,
    "imageURL": newProduct.imageURL,
    "visible": true,
  });
}

Future<void> updateProduct(Product product, BuildContext context) async {
  if(product.id == null) return;
  if(product.imagePATH != null){  //Atualiza a URL, caso a imagem tenha sido alterada
    product.imageURL = await uploadImage(product.imagePATH!);
  }
  return await FirebaseFirestore.instance
      .collection("products")
      .doc(product.id.toString())
      .update({
        "name": product.name,
        "price": product.price,
        "description": product.description,
        "imageURL": product.imageURL,
  });
}

Future<void> deleteProduct(int id, BuildContext context) async {
  return await FirebaseFirestore.instance
      .collection("products")
      .doc(id.toString())
      .update({
    "visible": false,
  });
}

Future<List<Product>> getProductsFromDB() async {
  List<Product> products = [];
  await FirebaseFirestore.instance
      .collection("products")
      .where('visible', isEqualTo: true)
      .orderBy('name')
      .get().then((QuerySnapshot querySnapshot) {
    for (var element in querySnapshot.docs) {
      products.add(docToProduct(element));
    }
  });
  return products;
}

Future<String> uploadImage(String filePath) async{
  File image = File(filePath);
  final storageRef = FirebaseStorage.instance.ref();
  await storageRef
    .child("images/" + basename(filePath))
    .putFile(image);
  return await storageRef.child("images/" + basename(filePath)).getDownloadURL();
}

var getProductFutureFirebaseSnapshot = FirebaseFirestore.instance
    .collection("products")
    .where('visible', isEqualTo: true)
    .orderBy('name')
    .get();

var getProductStreamFirebaseSnapshot = FirebaseFirestore.instance
    .collection("products")
    .where('visible', isEqualTo: true)
    .orderBy('name')
    .snapshots();