import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:emporio_do_diabetico/app/repository/Shared.dart';
import 'package:flutter/cupertino.dart';
import '../models/User.dart';

/*
  class User {
    int? id;
    String? name;
    String? email;
    String? cpf;
    String? phoneNumber;
    String? password;
    DateTime? registrationDate;
  }
*/

Future<void> addUser(User newUser, BuildContext context) async {
  int id = await whoIsNextCollectionId("users");

  var bytes = utf8.encode(newUser.password ?? ""); // data being hashed
  var digest = sha1.convert(bytes);
  return await FirebaseFirestore.instance.collection("users").doc(id.toString()).set({
    "id": id,
    "name": newUser.name,
    "email": newUser.email,
    "cpf": newUser.cpf,
    "phoneNumber": newUser.phoneNumber,
    "password": digest.toString(),
    "registrationDate": newUser.registrationDate,
    "visible": true,
    "isAdmin" : newUser.isAdmin
  });
}

Future<void> updateUser(User user, BuildContext context) async {
  if(user.id == null) return;
  return await FirebaseFirestore.instance
      .collection("users")
      .doc(user.id.toString())
      .update({
        "name": user.name,
        "email": user.email,
        "cpf": user.cpf,
        "phoneNumber": user.phoneNumber,
        "password": user.password,
  });
}

Future<void> deleteUser(int id, BuildContext context) async {
  return await FirebaseFirestore.instance
      .collection("users")
      .doc(id.toString())
      .update({
    "visible": false,
  });
}

Future<List<User>> getUsersFromDB() async {
  List<User> products = [];
  await FirebaseFirestore.instance
      .collection("users")
      .where('visible', isEqualTo: true)
      .orderBy('name')
      .get().then((QuerySnapshot querySnapshot) {
    for (var element in querySnapshot.docs) {
      products.add(docToUser(element));
    }
  });
  return products;
}

var getUserFutureFirebaseSnapshot = FirebaseFirestore.instance
    .collection("users")
    .where('visible', isEqualTo: true)
    .orderBy('name')
    .get();

Future<User?> login(String email, String password) async {
  var bytes = utf8.encode(password);
  var digest = sha1.convert(bytes);

  return await FirebaseFirestore.instance
      .collection("users")
      .where("email", isEqualTo: email)
      .where("password", isEqualTo: digest.toString())
      .where("visible", isEqualTo: true)
      .get()
      .timeout(const Duration(seconds: 8))
      .then((QuerySnapshot snapshot) {
        if (snapshot != null && snapshot.docs.isNotEmpty) {
          print(snapshot.docs[0]);
          User user = docToUser(snapshot.docs[0]);
          print(user);
          return user;
        }
      });
}