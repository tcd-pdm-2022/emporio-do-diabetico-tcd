import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import '../models/Product.dart';

Future<int> whoIsNextCollectionId(String collection) async {
  var snapshot = (await FirebaseFirestore.instance
      .collection(collection)
      .orderBy("id", descending: true)
      .limit(1)
      .get());
  int nextID = 1;
  if (snapshot.docs.isNotEmpty) {
    for (var element in snapshot.docs) {
      nextID = ((element['id']) + 1);
    }
  }
  return nextID;
}