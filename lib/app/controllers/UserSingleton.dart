import '../models/User.dart';


class SingletonUser {
  User? user;

  static SingletonUser? _instance;

  factory SingletonUser({User? user}) {
    if (_instance == null) {
      _instance = SingletonUser._internalConstructor(user);
    }
    _instance ?? SingletonUser._internalConstructor(user);
    return _instance!;
  }

  SingletonUser._internalConstructor(this.user);
}