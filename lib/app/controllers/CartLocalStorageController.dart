import 'dart:convert';
import 'dart:io';
import 'package:emporio_do_diabetico/app/models/CartItem.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';

Future<File> _getFile() async {
  final directory = await getApplicationDocumentsDirectory();
  var file = File("${directory.path}/data.json");
  if(file.existsSync()) {
    return file;
  } else {
    await File('${directory.path}/data.json').create(recursive: true);
    file = File("${directory.path}/data.json");
    return file;
  }
}

Future<File> saveData(List<CartItem> _CartItems) async {
  Map<String,dynamic> cartItemsMap = Map();
  for (var item in _CartItems) {
    cartItemsMap[item.productId.toString()] = item.quantity;
  }

  String data = json.encode(cartItemsMap);
  final file = await _getFile();
  return file.writeAsString(data);
}

Future<String> readData() async {
  try {
    final file = await _getFile();
    return file.readAsString();
  } catch (e) {
    return "";
  }
}

Future<void> addItemToCart(CartItem cartItem) async {
  List<CartItem> items = await getCartItems() ?? [];
  bool isProductInList = false;
  for (var element in items) {
    if(element.productId == cartItem.productId){
      element.quantity += cartItem.quantity;
      isProductInList = true;
    }
  }
  if(!isProductInList)
    items.add(cartItem);
  saveData(items);
}

Future<List<CartItem>?> getCartItems() async {
  List<CartItem> items = [];
  Map<String,dynamic> cartItemsMap = Map();
  var data = await readData();
  if(data != ""){
    cartItemsMap = json.decode(data);
    cartItemsMap.forEach((key, value) {
      items.add(CartItem(int.parse(key), value));
    });
  }
  return items;
}

Future<void> cleanCart() async{
  List<CartItem> items = [];
  await saveData(items);
}